'use strict';

/**
 * @ngdoc function
 * @name worldChampionsApp.controller:WinnersCtrl
 * @description
 * # WinnersCtrl
 * Controller of the worldChampionsApp
 */
angular.module('worldChampionsApp')
  .controller('WinnersCtrl', function ($scope, $stateParams, APIService) {
    $scope.champion = {};
    $scope.season = $stateParams.season;

    function highlightChampion(season) {
      APIService.getChampionOfSeason(season).then(function(champion) {
        $scope.champion = champion;
      });
    }

    APIService.getSeasonWinners($stateParams.season)
      .then(function(winners) {
        $scope.winners = winners.data.MRData.RaceTable.Races;
        highlightChampion($stateParams.season);
      });
  });
