'use strict';

/**
 * @ngdoc function
 * @name worldChampionsApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the worldChampionsApp
 */
angular.module('worldChampionsApp')
  .controller('HomeCtrl', function ($scope, $state, APIService) {
    $scope.showSeasonWinners = showSeasonWinners;

    APIService.getStandings()
      .then(function(standings) {
        $scope.standings = standings;
      });

     function showSeasonWinners(season) {
      $state.go('winners', {season: season});
    } 

  });
