'use strict';

/**
 * @ngdoc service
 * @name worldChampionsApp.APIService
 * @description
 * # APIService
 * Service in the worldChampionsApp.
 */
angular.module('worldChampionsApp')
  .service('APIService', function ($http, $q) {
    var _standings = [];

    function apiGetErgastDriverStadings() {
      return $http({
        url: 'http://ergast.com/api/f1/driverstandings/1.json?limit=11&offset=55'
      });
    }

    function apiGetChampionOfSeason(season) {
      var url = 'http://ergast.com/api/f1/{season}/driverstandings/1.json';
      url = url.replace(/{season}/, season);
      return $http({
        url: url
      });
    }

    return {
      getStandings: function() {
        const deferred = $q.defer();
        if (_standings.length) {
          deferred.resolve(_standings);
        } else {
          apiGetErgastDriverStadings()
            .then(function(res) {
              _standings = res.data.MRData.StandingsTable.StandingsLists;
              deferred.resolve(_standings);
            })
            .catch(deferred.reject);
        }
        return deferred.promise;
      },

      getSeasonWinners: function(season) {
        var url = 'http://ergast.com/api/f1/{season}/results/1.json';
        url = url.replace(/{season}/, season);
        return $http({
          url: url
        });
      },

      getChampionOfSeason: function(season) {
        const deferred = $q.defer();
        if (_standings.length) {
          var championData = _standings.find(function(v) {
            return v.season === season;
          });
          championData = (championData) ? championData.DriverStandings[0].Driver : {};
          deferred.resolve(championData);
        }
        else {
          apiGetChampionOfSeason(season).then(function(res) {
            var driverData = res.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver;
            deferred.resolve(driverData);
          })
          .catch(deferred.reject)
        }
        return deferred.promise;
      }
    };
  });
