'use strict';

/**
 * @ngdoc overview
 * @name worldChampionsApp
 * @description
 * # worldChampionsApp
 *
 * Main module of the application.
 */
angular
  .module('worldChampionsApp', [
    'ngAnimate',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router'
  ])
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('home', {
      url: '/',
      controller: 'HomeCtrl',
      templateUrl: 'views/home.html'
    })
    .state('winners', {
      url: '/winners/:season',
      controller: 'WinnersCtrl',
      templateUrl: 'views/winners.html'
    });
    $urlRouterProvider.when('', '/');
  });
