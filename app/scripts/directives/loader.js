'use strict';

/**
 * @ngdoc directive
 * @name worldChampionsApp.directive:loader
 * @description
 * # loader
 */
angular.module('worldChampionsApp')
  .directive('loader', function () {
    return {
      template: '<div class="loader-wrapper"><div class="loader"></div></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
      }
    };
  });
