# world-champions

This project was made on Angualr for Mobiquity technical test.

## About the structure
This project was generate using [Yeoman Angular Generator](https://github.com/yeoman/generator-angular). I took that decision because it gives me rapid development and some automated task. But for Angular production apps I prefer make some changes in order to get a [more professional pattern](https://github.com/johnpapa/angular-styleguide).

I am using SMACSS as a CSS pattern. Since this is not a large project, I just organized stylesheets as module files, instead of create specificals folders.

## Dependencies

- [Node.js v6+](https://nodejs.org)
- [NPM v3+](https://www.npmjs.com)

Before make sure that you have Node.js and NPM, it's time to install Bower and Gulp as globals depedencies running:
```sh
npm install -g grunt-cli bower
```
## Up and running

To run this project you had to follow these steps below:

### 1 - Install Angular Dependencies
Clone the repository
```sh
git clone MacgyverMartins@bitbucket.org:MacgyverMartins/mobiquity-ergast.git && cd mobiquity-ergast
```
Install Angular dependencies. It will take a few minutes
```sh
npm i && bower i 
```

### 2 - Build and run
Run `gulp build` to build the project and output de `/dist` folder
```sh
gulp build 
```
Then, run `gulp serve:prod` to simulate de production server
```sh
gulp serve:prod 
```
Now, navigate to `http://localhost:8081/` and there it is!

## Development
To run this project in development mode, just follow **step 1** and then run `gulp server`.
This command will initiate a simple server with a couple of automated tasks.
